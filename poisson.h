#pragma once
#include <cstdlib>
#include <random>

std::default_random_engine generator;
std::uniform_real_distribution<float> distribution(0.0,1.0);

struct Sample2
{
	Sample2(float x_, float y_) :
		x(x_), y(y_) {}
	float x, y;
};

struct GridCell
{
	GridCell() :
		offset(0), length(0) {}
	int offset;
	int length;
};

inline float CostFunction(float dist, float r_max, float alpha=8) {
	return powf(1.0f - std::min(dist, 2.f * r_max) / (2.f * r_max), alpha);
}

inline float rnd() {
	return distribution(generator);
}

inline float dist2(const Sample2& p0, const Sample2& p1) {
	float dx = p1.x - p0.x;
	float dy = p1.y - p0.y;
	return dx * dx + dy * dy;
}

void GetNeighborSamples(std::vector<int>* dst_vec,
	const std::vector<Sample2>& samples,
	const std::vector<GridCell>& grid_data,
	int grid_dim, int i, float d2_limit)
{
	dst_vec->clear();

	int x0 = floor(samples[i].x * grid_dim);
	int y0 = floor(samples[i].y * grid_dim);

	for (int dy = -1; dy < 2; ++dy) {
		for (int dx = -1; dx < 2; ++dx) {
			float x_offset = 0.f;
			float y_offset = 0.f;

			int x = x0 + dx;
			if (x < 0) {
				x += grid_dim;
				x_offset = -1.f;
			} else if (x >= grid_dim) {
				x -= grid_dim;
				x_offset = 1.f;
			}

			int y = y0 + dy;
			if (y < 0) {
				y += grid_dim;
				y_offset = -1.f;
			} else if (y >= grid_dim) {
				y -= grid_dim;
				y_offset = 1.f;
			}

			int c_idx = y * grid_dim + x;
			for (int u = grid_data[c_idx].offset; u < grid_data[c_idx].offset + grid_data[c_idx].length; ++u)
			{
				if(i == u)
					continue;

				Sample2 s_u(samples[u].x + x_offset, samples[u].y + y_offset);
				float d2 = dist2(samples[i], s_u);
				if (d2 < d2_limit)
					dst_vec->push_back(u);
			}
		}
	}

/*
	for (size_t u = 0; u < samples.size(); ++u) {
		if (i == u)
			continue;

		float d2 = dist2(samples[i], samples[u]);
		if (d2 < d2_limit)
			dst_vec->push_back(u);
	}
	*/
}

inline void RemoveSample(std::vector<GridCell>* grid_data,
	std::vector<Sample2>* samples,
	int grid_dim, int i)
{
	/*
	// Copy from back and pop
	(*samples)[i] = samples->back();
	samples->pop_back();
	*/

	int x = floor((*samples)[i].x * grid_dim);
	int y = floor((*samples)[i].y * grid_dim);

	int c_idx = y * grid_dim + x;
	assert((*grid_data)[c_idx].length > 0);

	int l_last = (*grid_data)[c_idx].offset + (*grid_data)[c_idx].length - 1;
	(*samples)[i] = (*samples)[l_last];
	--(*grid_data)[c_idx].length;
}

void BuildUniformGrid(std::vector<GridCell>* grid_data,
	std::vector<Sample2>* samples,
	int grid_dim, bool copy_old_grid = false)
{
	int size = (int)samples->size();
	std::vector<int> l_idx(size, 0);
	std::vector<int> g_idx(size, 0);
	std::vector<Sample2> samples_copy(samples->begin(), samples->end());

	grid_data->resize(grid_dim * grid_dim);
	for (int i = 0; i < grid_dim * grid_dim; ++i)
		(*grid_data)[i] = GridCell();

	for (int i = 0; i < size; ++i) {
		int x = floor(samples_copy[i].x * grid_dim);
		int y = floor(samples_copy[i].y * grid_dim);
		int c_idx = y * grid_dim + x;
		g_idx[i] = c_idx;
		l_idx[i] = (*grid_data)[c_idx].length++;
	}

	for (int i = 1; i < grid_dim * grid_dim; ++i)
		(*grid_data)[i].offset = (*grid_data)[i-1].offset + (*grid_data)[i-1].length;

	for (int i = 0; i < size; ++i) {
		int g_i = (*grid_data)[g_idx[i]].offset;
		int l_i = l_idx[i];
		int dst = g_i + l_i;
		(*samples)[dst] = samples_copy[i];
	}
}

void GeneratePoissonSamples(std::vector<Sample2>& samples, int n)
{
	const float r_max = sqrtf(1.f / (2.f * sqrtf(3.f) * n));
	const float d_limit = 2.f * r_max;
	const float d2_limit = d_limit * d_limit;
	const int start_samples = 4 * n;
	const int grid_dim = floor(1.f / d_limit) + 1;

	std::vector<Sample2> temp_samples;
	temp_samples.reserve(start_samples);

	std::vector<int> neighbor_samples;
	neighbor_samples.reserve(64);

	// Insert uniform random samples
	for (int i = 0; i < start_samples; ++i)
		temp_samples.push_back(Sample2(rnd(), rnd()));

	std::vector<GridCell> grid_data;
	BuildUniformGrid(&grid_data, &temp_samples, grid_dim);

	int num_samples = start_samples;
	//while (samples.size() > n) {
	while (num_samples-- > n) {
		float w_max = 0.f;
		size_t idx_max = 0;

		// Update weights and keep track of largest
		for (int c_i = 0; c_i < grid_dim * grid_dim; ++c_i) {
			for (int i = grid_data[c_i].offset; i < grid_data[c_i].offset + grid_data[c_i].length; ++i) {
				GetNeighborSamples(&neighbor_samples, temp_samples, grid_data, grid_dim, i, d2_limit);

				float w = 0.f;
				for (size_t u = 0; u < neighbor_samples.size(); ++u) {
					int o = neighbor_samples[u];
					w += CostFunction(sqrtf(dist2(temp_samples[i], temp_samples[o])), r_max);
				}

				if (w > w_max) {
					w_max = w;
					idx_max = i;
				}
			}
		}
		/*
		for (size_t i = 0; i < samples.size(); ++i) {
			GetNeighborSamples(&neighbor_samples, samples, grid_data, grid_dim, i, d2_limit);

			float w = 0.f;
			for (size_t u = 0; u < neighbor_samples.size(); ++u) {
				int o = neighbor_samples[u];
				w += CostFunction(sqrtf(dist2(samples[i], samples[o])), r_max);
			}

			if (w > w_max) {
				w_max = w;
				idx_max = i;
			}
		}
		*/

		RemoveSample(&grid_data, &temp_samples, grid_dim, idx_max);
	}

	samples.clear();
	samples.reserve(num_samples);

	for (int c_i = 0; c_i < grid_dim * grid_dim; ++c_i) {
		for (int i = grid_data[c_i].offset; i < grid_data[c_i].offset + grid_data[c_i].length; ++i) {
			samples.push_back(temp_samples[i]);
		}
	}
}