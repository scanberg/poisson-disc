#include <vector>
#include <glm/glm.hpp>
#include "poisson.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

int main()
{
	std::vector<Sample2> points;

	GeneratePoissonSamples(points, 1024);
	const int w = 1024;
	const int h = 1024;

	unsigned char* data = new unsigned char[w*h];

	for (int i = 0; i < w*h; ++i) {
		data[i] = 0;
	}

	for (int i = 0; i < (int)points.size(); ++i) {
		int x0 = (int)(points[i].x * w);
		int y0 = (int)(points[i].y * h);

		const int r = 16;
		for (int dy = -r; dy < r + 1; ++dy) {
			for(int dx = -r; dx < r + 1; ++dx) {
				int x = x0 + dx;
				x = (x < 0) ? 0 : (x >= w) ? w - 1 : x;
				int y = y0 + dy;
				y = (y < 0) ? 0 : (y >= h) ? y - 1 : y;

				float d2 = (float)(dx * dx + dy * dy);
				float d = (float)r - sqrtf(d2);
				if (d < 0.f) d = 0.f;
				unsigned char intensity = (int)(d * 255.f);
				data[y*w + x] = intensity;
			}
		}
	}

	stbi_write_png("bild.png", w, h, 1, data, 1024);
	delete[] data;

	return 0;
}